﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreTracker : MonoBehaviour {

	private int score;
	public static ScoreTracker Instance;
	public Text ScoreText;
//	public Text HighScoreText;

	public int Score
	{
		get
		{
			return score;
		}

		set
		{
			score = value;
			ScoreText.text = score.ToString();
		}
	}

	void Awake()
	{
		Instance = this;
        
	}

    // string user, int score, int[] tiles 
    public void SendData()
    {
    }

}
