﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Threading;

public class DataManagement : MonoBehaviour
{

    private string Name;
    private int Score;
    private string Email;
    private Dictionary<string, int> Products;
    private static string dirPath;
    string response;
    
    string permissionURL = "http://kdoge.lamp.ts/savepost/permission.php";
    string sendURL = "http://kdoge.lamp.ts/savepost/savepost.php";

    

    void Awake()
    {
        dirPath = string.Concat(Application.dataPath, "/Settings/");
    }

    // Set Name of Player
    public void SetName(string name)
    {
        Name = name;
    }
    // Set Player's Score
    public void SetScore(int score)
    {
        Score = score;
    }
    // Set Email
    public void SetEmail(string email)
    {
        Email = email;
    }
    public string GetName()
    {
        return Name;
    }
    public int GetScore()
    {
        return Score;
    }

    // Set Dictionary of Products
    public void SetTileValues(Tile[,] allTiles) // Find out what's wrong with this
    {
        Products = new Dictionary<string, int>();
        foreach (Tile myTile in allTiles)
        {
            string product = myTile.GetProduct(myTile);
            if (Products.ContainsKey(product))
                Products[product] += 1;
            else
                Products.Add(product, 1);
        }
    }

    public delegate void mySetup();
    public void AbleToPlay(mySetup setupFunc)
    {
        StartCoroutine(SendEmail(setupFunc));
        
    }
   
    bool TryGetResponse(string resp)
    {
        if (resp == "grant") return true;
        else return false;
    }

    public bool SendResponse()
    {
        return TryGetResponse(response);
    }

    IEnumerator SendEmail(mySetup setupFunc)
    {
        WWWForm form = new WWWForm();
        form.AddField("Email", Email);
        WWW www = new WWW(permissionURL, form);
        StartCoroutine(GetResponse(setupFunc));
        yield return www;
    }

    IEnumerator GetResponse(mySetup setupFunc)
    {
        WWW www = new WWW(permissionURL);
        StartCoroutine(WaitForRequest(setupFunc, www));
        //response = www.text;
        yield return null;
        //TryGetResponse(response);
    }

    IEnumerator WaitForRequest(mySetup setupFunc, WWW www)
    {
        while (!www.isDone)
        {

        }

        yield return www;
        response = www.text;
        setupFunc();
        // check for errors
        //if (www.error == null)
        //{
        //    response = www.text;
        //  //  Debug.Log("WWW Ok!: " + www.data);
        //}
        //else {
        //    while (!www.isDone)
        //    {

        //    }
        //  //  Debug.Log("WWW Error: " + www.error);
        //}
    }

    public bool isDone(WWW www)
    {
        if (www.isDone)
            return true;
        return false;
    }

    // magic xml shit
    public void CreatePlayerXML()
    {
        if (!System.IO.Directory.Exists(dirPath))
            System.IO.Directory.CreateDirectory(dirPath);

        string xmlFileName = Name + ".xml";
        string filePath = dirPath + xmlFileName; // File Path


        if (!File.Exists(filePath))
        {
            WriteFile(filePath);
        }
    }

    void SendEmail(string destinationUrl, string email)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
        byte[] bytes;
        bytes = System.Text.Encoding.ASCII.GetBytes(email);
        request.ContentType = "text/xml; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
    }

    public string postXMLData(string destinationUrl, string requestXml)
    {
        StreamReader reader = new StreamReader(requestXml);
        string ret = reader.ReadToEnd();
        reader.Close();

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
        byte[] bytes;
        bytes = System.Text.Encoding.ASCII.GetBytes(ret);
        request.ContentType = "text/xml; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();
        HttpWebResponse response;
        response = (HttpWebResponse)request.GetResponse();
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream responseStream = response.GetResponseStream();
            string responseStr = new StreamReader(responseStream).ReadToEnd();
            return responseStr;
        }
        return null;
    }

    void WriteFile(string filePath)
    {
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.NewLineOnAttributes = true;

        using (XmlWriter writer = XmlWriter.Create(filePath, settings))
        {
            // XmlDocument doc = new XmlDocument(); // new XML Doc

            writer.WriteStartDocument(); // Start XML Document
            writer.WriteStartElement("Record"); // Start Record Element
            writer.WriteStartElement("Player"); // Start Player Element
            writer.WriteElementString("Nickname", Name); // Player Name
            writer.WriteElementString("Email", Email);
            writer.WriteElementString("Score", Score.ToString()); // Player Score
            writer.WriteEndElement(); // End Player Element

            writer.WriteStartElement("Products"); // Start Products Element
                                                  //writer.WriteElementString("Test", "Test"); // Player Name
            foreach (KeyValuePair<string, int> kvp in Products)
            {
                writer.WriteElementString(kvp.Key, kvp.Value.ToString());
            }
            writer.WriteEndElement(); // End Products Element
            writer.WriteEndElement(); // End Record Element

            writer.WriteEndDocument(); // End XML Document    

            writer.Close();

            postXMLData(sendURL, filePath);

        }
    }
}