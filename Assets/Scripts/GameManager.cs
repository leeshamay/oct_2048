﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Timers;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public enum GameState
{
	Playing,
	GameOver, 
	WaitingForMoveToEnd
}

public class GameManager : MonoBehaviour
{

    #region Variables
    // 
    public Camera myCam;
    public Text myUser;
    public Text myEmail;
    private string user;
    private string email;
    private static Timer myTime = new Timer();
    //
    private static float timeLimit;
    private int theScore;

    // NEW AFTER ADDED DELAYS
    public GameState State;
    [Range(0, 2f)]
    public float delay;
    private bool moveMade;
    private bool[] lineMoveComplete = new bool[4] { true, true, true, true };
    // NEW AFTER ADDED DELAYS


    public GameObject YouWonText;
    public GameObject GameOverText;
    public Text GameOverScoreText;
    public GameObject GameOverPanel;
    public GameObject GameStartPanel;
    public GameObject AlreadyPlayedPanel;
    public Text Score;
    public GameObject Field;
    public SwipeManager SwipeManager;

    public DataManagement dataManager;

    private Tile[,] AllTiles = new Tile[4, 4];
    private List<Tile[]> columns = new List<Tile[]>();
    private List<Tile[]> rows = new List<Tile[]>();
    private List<Tile> EmptyTiles = new List<Tile>();
    
    #endregion

    bool bar = false;
    void SetFile()
    {
        if (bar) return;
        bar = true;

        // Set up the file
        int myScore;

        if (int.TryParse(GameOverScoreText.text, out myScore))
        {
            GetSetScore(myScore); // Get/Set Score
        }
        dataManager.SetTileValues(AllTiles);
        dataManager.CreatePlayerXML();
    }


    // Use this for initialization
    void Start()
    {
        // TODO: Change 5 to 60
        GameStartPanel.SetActive(true);
        Field.SetActive(false);

        timeLimit = 60f;
        SwipeManager.OnSwipeDetected += OnSwipeDetected;
        SetGame();
    }
    

    void OnSwipeDetected(Swipe direction)
    {
        if (direction == Swipe.Right) Move(MoveDirection.Right);
        if (direction == Swipe.Left) Move(MoveDirection.Left);
        if (direction == Swipe.Up) Move(MoveDirection.Up);
        if (direction == Swipe.Down) Move(MoveDirection.Down);
    }

    //Update is called once per frame
    void Update()
    {
        Score.text = theScore.ToString();
        if (timeLimit <= 0)
        {
            GameOver();
            myTime.Stop();
        }
        else
        {
            GameObject.Find("TitleText").GetComponent<Text>().text = "Time left: " + timeLimit.ToString("00") + " seconds";
        }
    }

    #region Timer
    private static void IHaveNoIdeaWhatImDoing()
    {
        //timeLimit = 5f;
        myTime.Interval = 1000;
        myTime.Enabled = true;
        myTime.Start();
    }

    private static void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e) // 'using System;' fixes this but breaks the 'Random' method used in another part of the code
    {
        timeLimit -= 1;
    }
    #endregion
    #region Game Setup

    public void SetGame()
    {
        if (State != GameState.GameOver) State = GameState.GameOver; // Makes sure the game is ended 
        if (GameOverPanel.activeSelf) GameOverPanel.SetActive(false);
        if (AlreadyPlayedPanel.activeSelf) AlreadyPlayedPanel.SetActive(false);
        if (!Field.activeSelf) Field.SetActive(true);
        GameStartPanel.SetActive(true);
    }

    public void CanStart()
    {
        string emailline = myEmail.text;
        string username = myUser.text;
        if (username != null && username != "" && emailline != null && emailline != "")
        {
            user = username;
            email = emailline;
            GetSetUser(this.user); // Get/Set user
            GetSetEmail(this.email); // Get/Set email
            dataManager.AbleToPlay(Setup);
        }
    }

    IEnumerator Wait()
    {
        //yield return null;
        yield return new WaitForSeconds(2);
        Setup();
    }

    

    void Setup()
    {
        Debug.Log("Starting Setup");
        Debug.Log(dataManager.SendResponse());
        if (dataManager.SendResponse())
        {
            GameStartPanel.SetActive(false);
            StartGame();
            Debug.Log("Starting");
        }
        else
        {
            GameStartPanel.SetActive(false);
            AlreadyPlayedPanel.SetActive(true);
        }
    }

    void StartGame()
    {
        GameOverPanel.SetActive(false);
        Tile[] AllTilesOneDim = GameObject.FindObjectsOfType<Tile>();
        foreach (Tile t in AllTilesOneDim)
        {
            t.Number = 0;
            AllTiles[t.indRow, t.indCol] = t;
            EmptyTiles.Add(t);
        }
        columns.Add(new Tile[] { AllTiles[0, 0], AllTiles[1, 0], AllTiles[2, 0], AllTiles[3, 0] });
        columns.Add(new Tile[] { AllTiles[0, 1], AllTiles[1, 1], AllTiles[2, 1], AllTiles[3, 1] });
        columns.Add(new Tile[] { AllTiles[0, 2], AllTiles[1, 2], AllTiles[2, 2], AllTiles[3, 2] });
        columns.Add(new Tile[] { AllTiles[0, 3], AllTiles[1, 3], AllTiles[2, 3], AllTiles[3, 3] });

        rows.Add(new Tile[] { AllTiles[0, 0], AllTiles[0, 1], AllTiles[0, 2], AllTiles[0, 3] });
        rows.Add(new Tile[] { AllTiles[1, 0], AllTiles[1, 1], AllTiles[1, 2], AllTiles[1, 3] });
        rows.Add(new Tile[] { AllTiles[2, 0], AllTiles[2, 1], AllTiles[2, 2], AllTiles[2, 3] });
        rows.Add(new Tile[] { AllTiles[3, 0], AllTiles[3, 1], AllTiles[3, 2], AllTiles[3, 3] });

        State = GameState.Playing;
        myTime.Elapsed += OnTimedEvent;
        IHaveNoIdeaWhatImDoing();
        Generate();
        Generate();
    }

    private void GameOver()
    {
   
        // States and Canvases
        State = GameState.GameOver;
        SwipeManager.OnSwipeDetected -= OnSwipeDetected;
        GameOverPanel.SetActive(true);
        myTime.Elapsed -= OnTimedEvent;

        // Text
        GameOverScoreText.text = Score.text;        
        Debug.Log(dataManager.GetName() + ", " + dataManager.GetScore());
        SetFile();
    }
    
    public void NewGameButtonHandler()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);    
    }
    #endregion
    #region Tiles
    bool CanMove()
    {
        if (EmptyTiles.Count > 0)
            return true;
        else
        {
            // check columns
            for (int i = 0; i < columns.Count; i++)
                for (int j = 0; j < rows.Count - 1; j++)
                    if (AllTiles[j, i].Number == AllTiles[j + 1, i].Number)
                        return true;

            // check rows
            for (int i = 0; i < rows.Count; i++)
                for (int j = 0; j < columns.Count - 1; j++)
                    if (AllTiles[i, j].Number == AllTiles[i, j + 1].Number)
                        return true;

        }
        return false;
    }
    bool MakeOneMoveDownIndex(Tile[] LineOfTiles)
    {
        myCam.cullingMask = 0;
        myCam.clearFlags = 0;
        for (int i = 0; i < LineOfTiles.Length - 1; i++)
        {
            //MOVE BLOCK 
            if (LineOfTiles[i].Number == 0 && LineOfTiles[i + 1].Number != 0)
            {
                LineOfTiles[i].Number = LineOfTiles[i + 1].Number;
                LineOfTiles[i + 1].Number = 0;
                return true;
            }
            // MERGE BLOCK
            if (LineOfTiles[i].Number != 0 && LineOfTiles[i].Number == LineOfTiles[i + 1].Number &&
                LineOfTiles[i].mergedThisTurn == false && LineOfTiles[i + 1].mergedThisTurn == false)
            {
                LineOfTiles[i].Number *= 2;
                LineOfTiles[i + 1].Number = 0;
                LineOfTiles[i].mergedThisTurn = true;
                LineOfTiles[i].PlayMergeAnimation();
                theScore += LineOfTiles[i].Number;
                return true;
            }
        }
        return false;
    }

    bool MakeOneMoveUpIndex(Tile[] LineOfTiles)
    {
        for (int i = LineOfTiles.Length - 1; i > 0; i--)
        {
            //MOVE BLOCK 
            if (LineOfTiles[i].Number == 0 && LineOfTiles[i - 1].Number != 0)
            {
                LineOfTiles[i].Number = LineOfTiles[i - 1].Number;
                LineOfTiles[i - 1].Number = 0;
                return true;
            }
            // MERGE BLOCK
            if (LineOfTiles[i].Number != 0 && LineOfTiles[i].Number == LineOfTiles[i - 1].Number &&
                LineOfTiles[i].mergedThisTurn == false && LineOfTiles[i - 1].mergedThisTurn == false)
            {
                        foreach (Tile thisTile in EmptyTiles)
        {
            Color color = new Color();
            ColorUtility.TryParseHtmlString("#D1D1D1FF", out color);
            thisTile.GetComponent<Image>().color = color;
        }
                LineOfTiles[i].Number *= 2;
                LineOfTiles[i - 1].Number = 0;
                LineOfTiles[i].mergedThisTurn = true;
                LineOfTiles[i].PlayMergeAnimation();
                theScore += LineOfTiles[i].Number;
                return true;
            }
        }
        return false;
    }

    void Generate()
    {
        string colorz = "";
        if (EmptyTiles.Count > 0)
        {
            int indexForNewNumber = Random.Range(0, EmptyTiles.Count);
            int randomNum = Random.Range(0, 10);
            if (randomNum == 0)
            {
                EmptyTiles[indexForNewNumber].Number = 4;
            }
            else
            {
                EmptyTiles[indexForNewNumber].Number = 2;
            }
            foreach (Tile thisTile in EmptyTiles)
            {
                Color color = new Color();
                ColorUtility.TryParseHtmlString("#D1D1D1FF", out color);
                thisTile.GetComponent<Image>().color = color;
            }
            EmptyTiles[indexForNewNumber].PlayAppearAnimation();
            EmptyTiles.RemoveAt(indexForNewNumber);
        }

    }
    private void ResetMergedFlags()
    {
        foreach (Tile t in AllTiles)
            t.mergedThisTurn = false;
    }

    private void UpdateEmptyTiles()
    {
        EmptyTiles.Clear();
        foreach (Tile t in AllTiles)
        {
            if (t.Number == 0)
                EmptyTiles.Add(t);
        }
    }
    #region Tile Movement

    public void Move(MoveDirection md)
    {
        //Debug.Log (md.ToString () + " move.");
        moveMade = false;
        ResetMergedFlags();
        if (delay > 0) StartCoroutine(MoveCoroutine(md));
        else
        {
            for (int i = 0; i < rows.Count; i++)
            {
                switch (md)
                {
                    case MoveDirection.Down:
                        while (MakeOneMoveUpIndex(columns[i]))
                        {
                            moveMade = true;
                        }
                        break;
                    case MoveDirection.Left:
                        while (MakeOneMoveDownIndex(rows[i]))
                        {
                            moveMade = true;
                        }
                        break;
                    case MoveDirection.Right:
                        while (MakeOneMoveUpIndex(rows[i]))
                        {
                            moveMade = true;
                        }
                        break;
                    case MoveDirection.Up:
                        while (MakeOneMoveDownIndex(columns[i]))
                        {
                            moveMade = true;
                        }
                        break;
                }
            }

            if (moveMade)
            {
                UpdateEmptyTiles();
                Generate();

                if (!CanMove())
                {
                    GameOver();
                }

            }
        }
    }

    IEnumerator MoveOneLineUpIndexCoroutine(Tile[] line, int index)
    {
        lineMoveComplete[index] = false;
        while (MakeOneMoveUpIndex(line))
        {
            moveMade = true;
            yield return new WaitForSeconds(delay);
        }
        lineMoveComplete[index] = true;
    }

    IEnumerator MoveOneLineDownIndexCoroutine(Tile[] line, int index)
    {
        lineMoveComplete[index] = false;
        while (MakeOneMoveDownIndex(line))
        {
            moveMade = true;
            yield return new WaitForSeconds(delay);
        }
        lineMoveComplete[index] = true;
    }

    IEnumerator MoveCoroutine(MoveDirection md)
    {
        while (State == GameState.WaitingForMoveToEnd)
        {
            yield return null;
        }

        State = GameState.WaitingForMoveToEnd;

        // start moving each line with delays depending on MoveDirection md
        switch (md)
        {
            case MoveDirection.Down:
                for (int i = 0; i < columns.Count; i++)
                    StartCoroutine(MoveOneLineUpIndexCoroutine(columns[i], i));
                break;
            case MoveDirection.Left:
                for (int i = 0; i < rows.Count; i++)
                    StartCoroutine(MoveOneLineDownIndexCoroutine(rows[i], i));
                break;
            case MoveDirection.Right:
                for (int i = 0; i < rows.Count; i++)
                    StartCoroutine(MoveOneLineUpIndexCoroutine(rows[i], i));
                break;
            case MoveDirection.Up:
                for (int i = 0; i < columns.Count; i++)
                    StartCoroutine(MoveOneLineDownIndexCoroutine(columns[i], i));
                break;
               
        }

        // Wait until the move is over in all lines
        while (!(lineMoveComplete[0] && lineMoveComplete[1] && lineMoveComplete[2] && lineMoveComplete[3]))
            yield return null;
        if (moveMade)
        {
            UpdateEmptyTiles();
            Generate();

            if (!CanMove())
            {
                GameOver();
            }

        }

        State = GameState.Playing;

        StopAllCoroutines();
    }
    #endregion
    #endregion
    #region Getters/Setters
    // Gets all the values for the tiles
    private void GetSetTileValues(Tile[,] tiles)
    {

    }

    // Get/Set user
    private void GetSetUser(string username)
    {
        dataManager.SetName(username);
    }

    private void GetSetEmail(string email)
    {
        dataManager.SetEmail(email);
    }

    // Get/Set score
    private void GetSetScore(int myScore)
    {
        dataManager.SetScore(myScore);
    }
    #endregion

}