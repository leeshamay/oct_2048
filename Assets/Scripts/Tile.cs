﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Tile : MonoBehaviour {

	public bool mergedThisTurn = false;
	 
	public int indRow;
	public int indCol;

	public int Number
	{
		get
		{
			return number;
		}
		set
		{
			number = value;
			if (number == 0)
				SetEmpty();
			else 
			{
				ApplyStyle(number);
				SetVisible();
			}
		}
	} private int number;

	private Text TileText;
	private Image TileImage;
	private Animator anim;
    public string Product;
    private Image backgroundImage;

	void Awake()
	{
		anim = GetComponent<Animator> ();
		TileText = GetComponentInChildren<Text> ();
		TileImage = transform.Find ("NumberedCell").GetComponent<Image> ();
	}
     
	public void PlayMergeAnimation()
	{
		anim.SetTrigger("Merge");
	}

	public void PlayAppearAnimation()
	{
		anim.SetTrigger("Appear");
	}

    public void PlaySlideAnimation()
    {
        anim.SetTrigger("Slide");
    }

    public string GetProduct(Tile tile) // this might be broken 
    {
        if (tile.Number >= 2)
            return tile.Product;
        return "EmptyTile";
    }

	void ApplyStyleFromHolder(int index)
	{
        
        TileImage.sprite = TileStyleHolder.Instance.TileStyles[index].TileSprite;
        TileText.text = "";
        this.GetComponent<Image>().color = TileStyleHolder.Instance.TileStyles[index].TileColor;
        Product = TileStyleHolder.Instance.TileStyles[index].Product;
	}

	void ApplyStyle(int num)
	{
        int twoByThePowerOfNum;
        twoByThePowerOfNum = (int)Mathf.Log(num, 2) - 1;
        ApplyStyleFromHolder(twoByThePowerOfNum);
	}

	private void SetVisible()
	{
		TileImage.enabled = true;
		TileText.enabled = true;
	}

	private void SetEmpty()
	{
		TileImage.enabled = false;
		TileText.enabled = false;
        Color color = new Color();
        ColorUtility.TryParseHtmlString("#D1D1D1FF", out color);
        this.GetComponent<Image>().color = color;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
